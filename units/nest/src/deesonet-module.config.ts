import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class DeesonetModuleConfig {
  public readonly tmpDir: string;
  public readonly tick: number;

  constructor(configs: ConfigService) {
    this.tmpDir = configs.get('DEESONET_TMP_DIR');
    this.tick = configs.get('DEESONET_TICK_MS') || 1000;
  }
}
