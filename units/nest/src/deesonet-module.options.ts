import { ApiClient, ApiClientConfig } from '@deesonet/common';

export type DeesonetModuleOption = {
  class: new (config: ApiClientConfig) => ApiClient;
  nestContextToken: string;
  name?: string;
};
