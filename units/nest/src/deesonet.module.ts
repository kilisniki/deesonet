import { DynamicModule, Module, Provider } from '@nestjs/common';
import { DeesonetModuleConfig } from './deesonet-module.config';
import { DeesonetModuleOption } from './deesonet-module.options';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MessageQueueService } from '@deesonet/common';

function getProvider(option: DeesonetModuleOption): Provider {
  return {
    provide: option.nestContextToken,
    inject: [ConfigService, DeesonetModuleConfig, MessageQueueService],
    useFactory: (
      configService: ConfigService,
      configs: DeesonetModuleConfig,
      messageQueue: MessageQueueService,
    ) => {
      const instance = new option.class({
        name: option.name || option.nestContextToken,
        token: configService.get(`${option.nestContextToken}_API_TOKEN`),
        tmpDir: configs.tmpDir,
      });
      messageQueue.addClient(instance);
      return instance;
    },
  };
}

@Module({})
export class DeesonetModule {
  static forRoot(options: DeesonetModuleOption[]): DynamicModule {
    const providers: Provider[] = options.map(getProvider);

    const messageQueue = {
      provide: MessageQueueService,
      inject: [DeesonetModuleConfig],
      useFactory: (configs: DeesonetModuleConfig) =>
        new MessageQueueService([], configs.tick),
    };

    return {
      module: DeesonetModule,
      imports: [ConfigModule],
      exports: [messageQueue, ...providers],
      providers: [DeesonetModuleConfig, messageQueue, ...providers],
    };
  }
}
