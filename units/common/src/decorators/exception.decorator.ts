/**
 * From https://javascript.plainenglish.io/a-try-catch-decorator-to-stylize-your-code-bdd0202765c8
 * */
import { ActionDataClass, AbstractBotExceptionClass } from '../classes';
import { DefaultErrorMessage } from '../constants';

/**
 * Этот декоратор обрабатывает выбрасываемые ошибки из метода, мапит их и отправляет сообщение клиенту.
 * @ТРЕБУЕТСЯ в контексте должен быть объект queue - экземпляр класса MessageQueueService
 * */
export const CatchBotException = (errorType: any = Error): any => {
  return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
    // Method decorator
    if (descriptor) {
      return _generateDescriptor(descriptor, errorType);
    }
    // Class decorator
    else {
      // Iterate over class properties except constructor
      for (const propertyName of Reflect.ownKeys(target.prototype).filter(
        (prop) => prop !== 'constructor',
      )) {
        const desc = Object.getOwnPropertyDescriptor(
          target.prototype,
          propertyName,
        )!;
        const isMethod = desc.value instanceof Function;
        if (!isMethod) continue;
        Object.defineProperty(
          target.prototype,
          propertyName,
          _generateDescriptor(desc, errorType),
        );
      }
    }
  };
};

function _handleError(
  ctx: any,
  errorType: any,
  error: Error,
  args: [ActionDataClass],
) {
  // Check if error is instance of given error type
  if (error instanceof errorType || true) {
    // Run handler with error object and class context
    ctx.queue.putMessage(args[0].authId, args[0].clientName, error.message);
  } else {
    console.error(error);
    console.error('ctx.queue');
    ctx.queue.putMessage(
      args[0].authId,
      args[0].clientName,
      DefaultErrorMessage,
    );
    // Throw error further
    // Next decorator in chain can catch it
    throw error;
  }
}

function _generateDescriptor(
  descriptor: PropertyDescriptor,
  errorType: any,
): PropertyDescriptor {
  // Save a reference to the original method
  const originalMethod = descriptor.value;

  // Rewrite original method with try/catch wrapper
  descriptor.value = function (...args: any[]) {
    try {
      const result = originalMethod.apply(this, args);

      // Check if method is asynchronous
      if (result && result instanceof Promise) {
        // Return promise
        return result.catch((error: any) => {
          // @ts-ignore
          _handleError(this, errorType, error, args);
        });
      }

      // Return actual result
      return result;
    } catch (error) {
      // @ts-ignore
      _handleError(this, errorType, error, args);
    }
  };

  return descriptor;
}
