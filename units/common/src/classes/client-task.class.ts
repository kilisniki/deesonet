import { MessageClass } from './message.class';

export class ClientTask {
  recipients: number[];
  message: MessageClass;
}
