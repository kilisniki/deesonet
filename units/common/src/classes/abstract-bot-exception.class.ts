import { MessageClass } from './message.class';

export abstract class AbstractBotExceptionClass {
  message: MessageClass;
}
