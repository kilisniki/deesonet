import { ActionDataClass } from './action-data.class';
import { ClientTask } from './client-task.class';
import { ActionFunction, ApiClientConfig } from '../typings';

export abstract class ApiClient {
  constructor(
    private readonly _config: ApiClientConfig,
    private readonly _provider: string,
  ) {}

  get name(): string {
    return this.config.name;
  }

  get provider(): string {
    return this._provider;
  }

  protected get config(): ApiClientConfig {
    return this._config;
  }

  abstract init(): void;
  // TODO переделать на userId, messages: string[]
  abstract sendMessage(task: ClientTask);
  abstract addMessageRoute(func: (data: ActionDataClass) => Promise<void>);
  abstract addCommandRoute(route: string | RegExp, func: ActionFunction);
}
