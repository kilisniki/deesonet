export class MessageClass {
  private _text: string;
  private _image: string;
  private _files: string[];

  constructor(text = '') {
    this._text = text;
  }

  get text(): string {
    return this._text;
  }

  set text(value: string) {
    this._text = value;
  }

  get image(): string {
    return this._image;
  }

  set image(value: string) {
    this._image = value;
  }

  get files(): string[] {
    return this._files;
  }

  set files(value: string[]) {
    this._files = value;
  }

  copy(): MessageClass {
    const c = new MessageClass(this.text);
    c.image = this.image;
    c.files = this.files;
    return c;
  }
}
