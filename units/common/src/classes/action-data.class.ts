import { MessageClass } from './message.class';

export class ActionDataClass {
  constructor(
    public readonly clientName: string,
    public readonly provider: string,
    public readonly authId: number,
    public readonly message: MessageClass,
    public readonly meta?: unknown,
  ) {}
}
