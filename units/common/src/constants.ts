import { MessageClass } from './classes';

export const DefaultErrorMessage = new MessageClass(
  'Произошла ошибка, попробуйте позже.',
);
