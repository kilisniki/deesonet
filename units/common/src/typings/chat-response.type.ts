import { MessageClass } from '../classes';

/*
 * Класс для библиотеки. Она ничего не знает о внешнем коде.
 * */
export class ChatResponse {
  recipients: {
    clientName: string;
    authId: number;
  }[];
  message: MessageClass;

  constructor(clientName: string, authId: number, message: string) {
    this.recipients = [{ clientName, authId }];
    this.message = new MessageClass(message);
  }
}
