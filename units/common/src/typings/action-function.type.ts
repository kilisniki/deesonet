import { ActionDataClass } from '../classes';

export type ActionFunction = (data: ActionDataClass) => Promise<void>;
