export type ApiClientConfig = {
  name: string;
  token: string;
  tmpDir?: string;
};
