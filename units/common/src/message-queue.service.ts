import { ApiClient, ClientTask, MessageClass } from './classes';
import { ChatResponse } from './typings';

export class MessageQueueService {
  clientMap: Map<string, ApiClient>;

  channels: Map<string, ClientTask[]>;

  private intervalId;

  constructor(clients: ApiClient[], private readonly tick: number) {
    this.clientMap = new Map();
    this.channels = new Map();
    clients.forEach((client) => {
      this.clientMap.set(client.name, client);
      this.channels.set(client.name, []);
    });
    this.start();
  }

  public addClient(client: ApiClient) {
    this.clientMap.set(client.name, client);
    this.channels.set(client.name, []);
  }

  /**
   * Этот метод необходим для массовой рассылки сообщений
   * */
  public putResponse(response: ChatResponse) {
    for (const recipient of response.recipients) {
      this.channels.get(recipient.clientName).push({
        message: response.message,
        recipients: [recipient.authId],
      });
    }
  }

  /**
   * Этот метод необходим для отправки одного сообщения конкретному лицу
   * */
  public putMessage(authId: number, clientName: string, message: MessageClass) {
    this.channels.get(clientName).push({
      message: message,
      recipients: [authId],
    });
  }

  // TODO есть куда оптимизировать
  private start() {
    this.intervalId = setInterval(async () => {
      for (const channel of this.channels.keys()) {
        const client = this.clientMap.get(channel);
        const queue = this.channels.get(channel);
        while (queue.length > 0) {
          await client.sendMessage(queue.shift());
        }
      }
    }, this.tick || 2000);
  }
}
