import { Controller, Get } from '@nestjs/common';

@Controller()
export class AppController {
  @Get('_checkme')
  checkMe(): string {
    return 'Good!';
  }
}
