import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';

const PORT = 4000;

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  await app.listen(PORT);
  app.useStaticAssets(join(__dirname, '../../..', 'static'));
  app.setBaseViewsDir(join(__dirname, '../../..', 'static'));
  app.setViewEngine('hbs');
  console.log('Application listen port', PORT);
}
bootstrap();

process.on('unhandledRejection', (reason, promise) => {
  console.log('Unhandled rejection at ', promise, reason);
  // process.exit(1)
});
