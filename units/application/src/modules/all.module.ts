import { Module } from '@nestjs/common';
import { WatermarkBotModule } from './domain/bots/watermark/watermark-bot.module';
import { DeesonetModule } from './domain/bots/deesonet/deesonet.module';

@Module({
  imports: [WatermarkBotModule, DeesonetModule],
})
export class AllModule {}
