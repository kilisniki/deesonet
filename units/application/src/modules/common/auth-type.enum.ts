export enum AuthTypeEnum {
  VK = 'vk',
  TELEGRAM = 'tg',
  WEB = 'web',
}
