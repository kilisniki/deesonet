import { AuthTypeEnum } from './auth-type.enum';

export function mapToAuthType(str: string): AuthTypeEnum {
  return str as AuthTypeEnum;
}
