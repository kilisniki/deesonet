import { MessageClass, AbstractBotExceptionClass } from '@deesonet/common';

export class NoImageInMessageError extends AbstractBotExceptionClass {
  constructor() {
    super();
    this.message = new MessageClass(`Пришли изображение!`);
  }

  message: MessageClass;
}
