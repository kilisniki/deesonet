import { MessageClass, AbstractBotExceptionClass } from '@deesonet/common';

export class IncorrectTextForEncodeError extends AbstractBotExceptionClass {
  message: MessageClass;

  constructor() {
    super();
    this.message = new MessageClass(
      `К сожалению, допустимы только английские буквы и цифры, а также спецсимволы.`,
    );
  }
}
