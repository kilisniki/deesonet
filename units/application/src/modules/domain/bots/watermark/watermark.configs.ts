import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TransformConfig } from 'digital-watermarking';

@Injectable()
export class WatermarkConfigs {
  public readonly renderConfig: TransformConfig;
  public readonly TMP_DIR: string;
  public readonly WATERMARK_TG_API_TOKEN: string;
  public readonly QUEUE_TICK: number;
  constructor(private configService: ConfigService) {
    this.renderConfig = {
      fontSize: 1.0,
      resultImageWidth: undefined,
      resultImageHeight: undefined,
      resultImageQuality: 95,
      startX: 20,
      startY: 30,
      thickness: 3,
    };
    this.TMP_DIR = configService.get('TMP_DIR');
    this.WATERMARK_TG_API_TOKEN = configService.get('WATERMARK_TG_API_TOKEN');
    this.QUEUE_TICK = Number(configService.get<number>('QUEUE_TICK') || 2000);
  }
}
