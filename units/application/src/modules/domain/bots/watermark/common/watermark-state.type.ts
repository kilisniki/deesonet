import { WatermarkStateStatusEnum } from './watermark-state.status.enum';

export type WatermarkStateType = {
  status: WatermarkStateStatusEnum;
  imageForEncodePath?: string;
  imageCompression?: boolean;
};
