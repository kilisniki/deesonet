import { Controller, Get, Param, Query } from '@nestjs/common';
import * as path from 'path';
import { WatermarkConfigs } from '../watermark.configs';
import { ActionDataClass, MessageClass, Providers } from '@deesonet/common';
import { AuthTypeEnum } from '../../../../common';
import { WatermarkActionService } from '../services/watermark-action.service';

@Controller('watermark')
export class WebControllerWatermark {
  constructor(
    private readonly service: WatermarkActionService,
    private readonly configs: WatermarkConfigs,
  ) {}

  @Get('/fullwork/:fileName')
  async work(
    @Param('fileName') fileName: string,
    @Query('message') text: string,
  ): Promise<string> {
    const originalFile = path.join(this.configs.TMP_DIR, fileName);
    const message = new MessageClass(text);
    message.image = originalFile;
    const actionData = new ActionDataClass(
      AuthTypeEnum.WEB,
      Providers.WEB,
      0,
      message,
    );
    await this.service.encode(actionData); // ставит статус на ожидание картинки
    await this.service.message(actionData); // добавляет картинку в статус, переводит в статус ожидания текста
    const response = await this.service.message(actionData); // кодирует
    return response.text;
  }

  @Get('/decode/:fileName')
  async decode(@Param('fileName') fileName: string): Promise<string> {
    const encodedFile = path.join(this.configs.TMP_DIR, fileName);
    const message = new MessageClass('');
    message.image = encodedFile;
    const actionData = new ActionDataClass(
      AuthTypeEnum.WEB,
      Providers.WEB,
      0,
      message,
    );
    await this.service.decode(actionData); // переключает статус в ожидание картинки
    const response = await this.service.message(actionData); // добавляет картинку, раскодирует
    return response.text;
  }
}
