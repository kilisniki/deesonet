import { Inject, Injectable } from '@nestjs/common';
import { TelegramClient } from '@deesonet/libs';
import { WatermarkConfigs } from '../watermark.configs';
import { WATERMARK_TG_CLIENT } from '../constants';
import { WatermarkActionService } from '../services/watermark-action.service';

@Injectable()
export class WatermarkTelegramController {
  constructor(
    private readonly config: WatermarkConfigs,
    @Inject(WATERMARK_TG_CLIENT) private readonly client: TelegramClient,
    private readonly actionService: WatermarkActionService,
  ) {
    this.configuration();
    this.client.init();
  }

  private configuration() {
    this.client.addCommandRoute(
      /\/start(.*)/,
      this.actionService.start.bind(this.actionService),
    );
    this.client.addCommandRoute(
      /\/help(.*)/,
      this.actionService.help.bind(this.actionService),
    );
    this.client.addCommandRoute(
      /\/encode(.*)/,
      this.actionService.encode.bind(this.actionService),
    );
    this.client.addCommandRoute(
      /\/decode(.*)/,
      this.actionService.decode.bind(this.actionService),
    );
    this.client.addMessageRoute(
      this.actionService.message.bind(this.actionService),
    );
  }
}
