import { Injectable } from '@nestjs/common';
import { WatermarkingService } from './watermarking.service';
import { WatermarkStateService } from './watermark-state.service';
import {
  ActionDataClass,
  MessageClass,
  MessageQueueService,
  CatchBotException,
} from '@deesonet/common';
import { WatermarkStateStatusEnum } from '../common/watermark-state.status.enum';
import { NoImageInMessageError } from '../error-messages/no-image-in-message.error';
import { MESSAGES } from '../common/watermark-messages.templates';
import { IncorrectTextForEncodeError } from '../error-messages/incorrect-text-for-encode.error';

@Injectable()
export class WatermarkActionService {
  constructor(
    private readonly watermark: WatermarkingService,
    private readonly states: WatermarkStateService,
    private readonly queue: MessageQueueService,
  ) {}

  @CatchBotException()
  public async start(data: ActionDataClass) {
    const message = new MessageClass(MESSAGES.getStartMessage());
    await this.queue.putResponse({
      recipients: [{ authId: data.authId, clientName: data.clientName }],
      message,
    });
    return message;
  }

  @CatchBotException()
  public async help(data: ActionDataClass) {
    const message = new MessageClass(MESSAGES.getHelpMessage());
    this.queue.putResponse({
      recipients: [{ authId: data.authId, clientName: data.clientName }],
      message,
    });
    return message;
  }

  @CatchBotException()
  public async encode(data: ActionDataClass) {
    this.states.set(data.clientName, data.authId, {
      status: WatermarkStateStatusEnum.WAITING_IMAGE_FOR_ENCODE,
    });
    const message = new MessageClass(MESSAGES.getUploadImageForEncodeMessage());
    this.queue.putResponse({
      recipients: [{ authId: data.authId, clientName: data.clientName }],
      message,
    });
    return message;
  }

  @CatchBotException()
  public async decode(data: ActionDataClass) {
    this.states.set(data.clientName, data.authId, {
      status: WatermarkStateStatusEnum.WAITING_IMAGE_FOR_DECODE,
    });
    const message = new MessageClass(MESSAGES.getUploadImageForDecodeMessage());
    this.queue.putResponse({
      recipients: [{ authId: data.authId, clientName: data.clientName }],
      message,
    });
    return message;
  }

  @CatchBotException()
  public async message(data: ActionDataClass): Promise<MessageClass> {
    const state = this.states.get(data.clientName, data.authId);
    let respMessage: MessageClass;
    switch (state?.status) {
      case WatermarkStateStatusEnum.NONE:
        if (data.message.image || data.message.files?.length)
          respMessage = await this.imageForDecode(data);
        else respMessage = await this.noneStatus(data);
        break;
      case WatermarkStateStatusEnum.WAITING_IMAGE_FOR_ENCODE:
        respMessage = await this.imageForEncode(data);
        break;
      case WatermarkStateStatusEnum.WAITING_IMAGE_FOR_DECODE:
        respMessage = await this.imageForDecode(data);
        break;
      case WatermarkStateStatusEnum.WAITING_MESSAGE:
        respMessage = await this.textForEncode(data);
        break;
      default:
        if (data.message.image || data.message.files?.length)
          respMessage = await this.imageForDecode(data);
        respMessage = await this.noneStatus(data);
    }
    await this.queue.putResponse({
      message: respMessage,
      recipients: [{ authId: data.authId, clientName: data.clientName }],
    });
    return respMessage;
  }

  private async noneStatus(data: ActionDataClass): Promise<MessageClass> {
    return new MessageClass(MESSAGES.getStartMessage());
  }

  private async imageForEncode(data: ActionDataClass): Promise<MessageClass> {
    if (data.message.image || data.message.files?.length) {
      this.states.set(data.clientName, data.authId, {
        status: WatermarkStateStatusEnum.WAITING_MESSAGE,
        imageForEncodePath: data.message.image || data.message.files[0],
        imageCompression: !!data.message.image,
      });
      return new MessageClass(MESSAGES.getEnterCodeMessage());
    } else {
      throw new NoImageInMessageError();
    }
  }

  private async imageForDecode(data: ActionDataClass): Promise<MessageClass> {
    if (data.message.image || data.message.files?.length) {
      const imagePath = data.message.image || data.message.files[0];
      const imageCompression = !!data.message.image;

      const pathToDecodedImg = await this.watermark.decode(
        imagePath,
        imageCompression,
      );
      this.states.set(data.clientName, data.authId, {
        status: WatermarkStateStatusEnum.NONE,
      });

      const message = new MessageClass(MESSAGES.getDoneDecodeMessage());
      if (imageCompression) message.image = pathToDecodedImg;
      else message.files = [pathToDecodedImg];

      return message;
    } else {
      throw new NoImageInMessageError();
    }
  }

  private async textForEncode(data: ActionDataClass): Promise<MessageClass> {
    if (
      !/^[a-zA-Z0-9!@#$%^&*()-_+=[\]{}`~,<>?\/\\;\s]+([a-zA-Z0-9!@#$%^&*()-_+=[\]{}`~,<>?\/\\;\s]*)$/.test(
        data.message?.text,
      )
    ) {
      const error = new IncorrectTextForEncodeError();
      return error.message;
    }

    const state = this.states.get(data.clientName, data.authId);
    const path = await this.watermark.encode(
      state.imageForEncodePath,
      data.message?.text,
      state.imageCompression,
    );

    const message = new MessageClass(MESSAGES.getDoneEncodeMessage());
    if (state.imageCompression) message.image = path;
    else message.files = [path];

    this.states.set(data.clientName, data.authId, {
      status: WatermarkStateStatusEnum.NONE,
    });

    return message;
  }
}
