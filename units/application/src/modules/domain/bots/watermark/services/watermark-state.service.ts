import { Injectable } from '@nestjs/common';
import { AuthTypeEnum } from '../../../../common';
import { WatermarkStateType } from '../common/watermark-state.type';

Injectable();
export class WatermarkStateService {
  private readonly states: Map<string, WatermarkStateType>;

  constructor() {
    this.states = new Map();
  }

  public get(clientName: string, key: number): WatermarkStateType {
    return this.states.get(`${clientName}_${key}`);
  }

  public set(clientName: string, key: number, state: WatermarkStateType): void {
    this.states.set(`${clientName}_${key}`, state);
  }
}
