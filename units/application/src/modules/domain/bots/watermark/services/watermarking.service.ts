import { Injectable } from '@nestjs/common';
import * as dw from 'digital-watermarking';
import * as path from 'path';
import * as crypto from 'crypto';
import { WatermarkConfigs } from '../watermark.configs';

@Injectable()
export class WatermarkingService {
  constructor(private readonly config: WatermarkConfigs) {}

  async encode(
    filePath: string,
    message: string,
    compression = true,
  ): Promise<string> {
    const fileName = crypto
      .createHash('md5')
      .update(Date.now().toString())
      .digest('hex');
    const format = compression ? 'jpg' : 'png';
    const newPath = path.join(
      this.config.TMP_DIR,
      `enCode_${fileName}.${format}`,
    );

    const start = Date.now();
    await dw.transformImageWithText(
      filePath,
      message,
      newPath,
      this.config.renderConfig,
    );
    console.log('encoded by', (Date.now() - start) / 1000, 'sec.');
    return newPath;
  }

  async decode(encoded: string, compression = true): Promise<string> {
    const fileName = crypto
      .createHash('md5')
      .update(Date.now().toString())
      .digest('hex');
    const format = compression ? 'jpg' : 'png';
    const newPath = path.join(
      this.config.TMP_DIR,
      `deCode_${fileName}.${format}`,
    );
    await dw.getTextFormImage(encoded, newPath);
    return newPath;
  }
}
