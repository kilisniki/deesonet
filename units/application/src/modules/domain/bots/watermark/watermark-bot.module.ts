import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { WebControllerWatermark } from './controllers/web.controller.watermark';
import { WatermarkConfigs } from './watermark.configs';
import { WatermarkingService } from './services/watermarking.service';
import { WatermarkActionService } from './services/watermark-action.service';
import { WatermarkStateService } from './services/watermark-state.service';
import { TelegramClient } from '@deesonet/libs';
import { WATERMARK_TG_CLIENT } from './constants';
import { WatermarkTelegramController } from './controllers/watermark.telegram.controller';
import { AuthTypeEnum } from '../../../common';
import { DeesonetModule } from '@deesonet/nest';

@Module({
  imports: [
    ConfigModule,
    DeesonetModule.forRoot([
      {
        class: TelegramClient,
        nestContextToken: WATERMARK_TG_CLIENT,
        name: AuthTypeEnum.TELEGRAM,
      },
    ]),
  ],
  controllers: [WebControllerWatermark],
  providers: [
    WatermarkConfigs,
    WatermarkingService,
    WatermarkActionService,
    WatermarkStateService,
    WatermarkTelegramController,
  ],
})
export class WatermarkBotModule {}
