import { AbstractBotExceptionClass, MessageClass } from '@deesonet/common';

export class ChatNotFoundException extends AbstractBotExceptionClass {
  constructor(chatId: number) {
    super();
    this.message = new MessageClass(`Чат с id <${chatId}> не найден`);
  }
}
