import {
  AbstractBotExceptionClass,
  MessageClass,
} from '@deesonet/common/build';

export class UnavailableLangError extends AbstractBotExceptionClass {
  constructor() {
    super();
    this.message = new MessageClass(`Данный язык не поддерживается.`);
  }

  message: MessageClass;
}
