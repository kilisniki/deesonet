import { ActorClass } from './actor.class';
import { MessageClass } from '@deesonet/common';

export class DeesonetResponse {
  recipients: ActorClass[];
  message: MessageClass;

  constructor(recipients: ActorClass[], message: MessageClass) {
    this.recipients = recipients;
    this.message = message;
  }
}
