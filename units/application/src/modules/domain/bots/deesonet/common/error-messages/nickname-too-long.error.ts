import { MessageClass, AbstractBotExceptionClass } from '@deesonet/common';
import { MAX_NICKNAME_LENGTH } from '../../constants';

export class NicknameTooLongError extends AbstractBotExceptionClass {
  constructor() {
    super();
    this.message = new MessageClass(
      `Максимальная длина никнейма ${MAX_NICKNAME_LENGTH} символов. Уважайте других участников!`,
    );
  }

  message: MessageClass;
}
