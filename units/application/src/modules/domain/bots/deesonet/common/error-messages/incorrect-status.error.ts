import {
  AbstractBotExceptionClass,
  MessageClass,
} from '@deesonet/common/build';

export class IncorrectStatusError extends AbstractBotExceptionClass {
  constructor() {
    super();
    this.message = new MessageClass(
      `Произошла ошибка, пожалуйста попробуйте выполнить операцию снова`,
    );
  }

  message: MessageClass;
}
