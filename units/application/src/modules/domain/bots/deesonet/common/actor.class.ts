import { AccountModel } from '../../../../infra/models/deesonet/account.model';
import { UserModel } from '../../../../infra/models/deesonet/user.model';
import { ProfileModel } from '../../../../infra/models/deesonet/profile.model';

export class ActorClass {
  account: AccountModel;
  user: UserModel;
  activeProfile?: ProfileModel;
  state?: boolean;
}
