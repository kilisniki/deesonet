import {
  AbstractBotExceptionClass,
  MessageClass,
} from '@deesonet/common/build';

export class ActiveProfileNotExistError extends AbstractBotExceptionClass {
  constructor() {
    super();
    this.message = new MessageClass(
      `У вас нет активного чата.\n/getchats - получить список чатов`,
    );
  }

  message: MessageClass;
}
