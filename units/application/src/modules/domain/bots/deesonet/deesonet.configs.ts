import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class DeesonetConfigs {
  public readonly TG_API_TOKEN: string;
  public readonly VK_API_TOKEN: string;
  public readonly TMP_DIR: string;
  public readonly QUEUE_TICK: number;
  public readonly YANDEX_API_KEY: string;

  constructor(private readonly configs: ConfigService) {
    this.TG_API_TOKEN = configs.get('TG_API_TOKEN');
    this.VK_API_TOKEN = configs.get('VK_API_TOKEN');
    this.TMP_DIR = configs.get('TMP_DIR');
    this.QUEUE_TICK = Number(configs.get<number>('QUEUE_TICK') || 2000);
    this.YANDEX_API_KEY = configs.get('YANDEX_API_KEY');
  }
}
