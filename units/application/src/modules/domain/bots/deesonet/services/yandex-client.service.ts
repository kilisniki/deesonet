import { Injectable } from '@nestjs/common';
import { DeesonetConfigs } from '../deesonet.configs';
import axios from 'axios';

@Injectable()
export class YandexClientService {
  private readonly cache = new Map<string, string>();
  constructor(private readonly config: DeesonetConfigs) {}

  public async translate(lang: 'en' | 'ru', text: string): Promise<string> {
    const hash = `${lang}:_:${text}`;
    const cached = this.cache.get(hash);
    if (cached) return cached;
    const result = await axios.post(
      'https://translate.api.cloud.yandex.net/translate/v2/translate',
      {
        targetLanguageCode: lang,
        format: 'PLAIN_TEXT',
        texts: [text],
      },
      {
        headers: {
          Authorization: `Api-key ${this.config.YANDEX_API_KEY}`,
        },
      },
    );
    return result.data?.translations[0]?.text || text;
  }
}
