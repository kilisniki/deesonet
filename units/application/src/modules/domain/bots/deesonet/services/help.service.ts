import { UserModel } from '../../../../infra/models/deesonet/user.model';
import { Repository } from 'typeorm';
import { ChatModel } from '../../../../infra/models/deesonet/chat.model';
import { ProfileModel } from '../../../../infra/models/deesonet/profile.model';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AccountModel } from '../../../../infra/models/deesonet/account.model';
import { AuthTypeEnum } from '../../../../common';
import { MessageModel } from '../../../../infra/models/deesonet/message.model';
import { ActorClass } from '../common/actor.class';
import { MessageClass } from '@deesonet/common';
import { ProfileSettings } from '../common';

@Injectable()
export class HelpService {
  constructor(
    @InjectRepository(UserModel) private usersRepository: Repository<UserModel>,
    @InjectRepository(ChatModel) private chatRepository: Repository<ChatModel>,
    @InjectRepository(ProfileModel)
    private profileRepository: Repository<ProfileModel>,
    @InjectRepository(AccountModel)
    private accountRepository: Repository<AccountModel>,
    @InjectRepository(MessageModel)
    private messageRepository: Repository<MessageModel>,
  ) {}

  public async getUser(
    authId: number,
    authType: AuthTypeEnum,
  ): Promise<UserModel> {
    const account = await this.getAccount(authId, authType);
    if (account) {
      return this.usersRepository.findOne(account.user.id);
    }
    return null;
  }

  public async getAccount(
    authId: number,
    authType: AuthTypeEnum,
  ): Promise<AccountModel> {
    return this.accountRepository.findOne(
      { authId, authType },
      {
        relations: ['user'],
      },
    );
  }

  public async getActor(
    authId: number,
    authType: AuthTypeEnum,
  ): Promise<ActorClass> {
    const account = await this.getAccount(authId, authType);
    const activeProfile = await this.getActiveProfileForUser(account.user);
    return {
      account,
      user: account.user,
      activeProfile,
    };
  }

  public async createUser(
    authId: number,
    authType: AuthTypeEnum,
    meta?: unknown,
  ): Promise<UserModel> {
    const user = await this.usersRepository.save(new UserModel());
    const newAccount = new AccountModel();
    newAccount.user = user;
    newAccount.authId = authId;
    newAccount.authType = authType;
    if (meta) {
      if (typeof meta === 'object') newAccount.meta = meta;
      else newAccount.meta = { data: meta };
    }
    await this.accountRepository.save(newAccount);
    return user;
  }

  public async getActiveProfileForUser(userId): Promise<ProfileModel> {
    return this.profileRepository.findOne(
      { user: userId, active: true },
      {
        relations: ['user', 'chat'],
      },
    );
  }

  public async getChats() {
    return this.chatRepository.find({ active: true });
  }

  public async getChat(chatId: number) {
    return this.chatRepository.findOne(chatId);
  }

  public async createChat(chatname: string) {
    const chat = new ChatModel();
    chat.name = chatname;
    return this.chatRepository.save(chat);
  }

  public async getProfile(userId: number, chatId: number) {
    // @ts-ignore
    return this.profileRepository.findOne({ user: userId, chat: chatId });
  }

  public async activateProfile(profileId: number) {
    const profile = await this.profileRepository.findOne(profileId);
    profile.active = true;
    return this.profileRepository.save(profile);
  }

  public async deactivateProfile(userId: number) {
    // @ts-ignore
    const activeProfiles = await this.profileRepository.find({
      user: userId,
      active: true,
    });
    for (const profile of activeProfiles) {
      profile.active = false;
      await this.profileRepository.save(profile);
    }
  }

  public async createProfile(chat: number, user: number, nickname: string) {
    const profile = new ProfileModel();
    // @ts-ignore
    profile.chat = chat;
    // @ts-ignore
    profile.user = user;
    profile.settings = new ProfileSettings();
    profile.nickname = nickname;
    profile.active = true;
    return this.profileRepository.save(profile);
  }

  public async setTranslatorForProfile(profileId: number, lang: string) {
    const profile = await this.profileRepository.findOne(profileId);
    if (lang.toLowerCase() === 'null') {
      profile.settings.translate = null;
    } else {
      // @ts-ignore
      profile.settings.translate = lang.toLowerCase();
    }
    return this.profileRepository.save(profile);
  }

  private async getRecipientsId(
    chatId: number,
    authorId: number,
  ): Promise<AccountModel[]> {
    const accounts = await this.accountRepository.find({
      relations: ['user', 'user.profiles', 'user.profiles.chat'],
    });
    return accounts.filter((el) => {
      return el.user.profiles.find(
        (p) => p.active && p.chat.id === chatId && p.id !== authorId,
      );
    });
  }

  public async getRecipients(
    chatId: number,
    authorId: number,
  ): Promise<ActorClass[]> {
    const accounts = await this.getRecipientsId(chatId, authorId);
    return Promise.all(
      accounts.map(async (el): Promise<ActorClass> => {
        const profile = await this.getActiveProfileForUser(el.user.id);
        return {
          account: el,
          user: el.user,
          activeProfile: profile,
        };
      }),
    );
  }

  public async saveMessage(
    data: MessageClass,
    account: AccountModel,
    profile: ProfileModel,
  ): Promise<MessageModel> {
    const message = new MessageModel();
    message.data = data.text;
    message.account = account;
    message.profile = profile;
    return this.messageRepository.save(message);
  }
}
