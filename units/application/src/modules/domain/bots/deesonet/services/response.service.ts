import { Injectable } from '@nestjs/common';
import { ChatResponse, MessageQueueService } from '@deesonet/common';
import { DeesonetResponse } from '../common';
import { YandexClientService } from './yandex-client.service';

@Injectable()
export class ResponseService {
  constructor(
    private readonly queue: MessageQueueService,
    private readonly yandex: YandexClientService,
  ) {}

  public async put(response: DeesonetResponse) {
    const responses = await this.updateChatResponse(response);
    responses.forEach((el) => this.queue.putResponse(el));
  }

  private async updateChatResponse(
    original: DeesonetResponse,
  ): Promise<ChatResponse[]> {
    const commonResponse: ChatResponse = {
      recipients: [],
      message: original.message,
    };
    const responses: ChatResponse[] = [commonResponse];
    for (const recipient of original.recipients) {
      if (recipient.activeProfile?.settings?.translate) {
        const message = original.message.copy();
        try {
          message.text = await this.yandex.translate(
            recipient.activeProfile.settings.translate,
            original.message.text,
          );
        } catch (e) {
          console.log(e);
        }
        responses.push({
          recipients: [
            {
              authId: recipient.account.authId,
              clientName: recipient.account.authType,
            },
          ],
          message,
        });
      } else {
        commonResponse.recipients.push({
          authId: recipient.account.authId,
          clientName: recipient.account.authType,
        });
      }
    }
    return responses;
  }
}
