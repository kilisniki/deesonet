import { Injectable } from '@nestjs/common';
import { HelpService } from './help.service';
import { MESSAGES } from '../messages';
import { AuthTypeEnum } from '../../../../common';
import { MessageClass } from '@deesonet/common';
import { NicknameTooLongError } from '../common/error-messages/nickname-too-long.error';
import { availableLang, MAX_NICKNAME_LENGTH } from '../constants';
import { ChatNotFoundException } from '../common/error-messages/chat-not-found.exception';
import {
  ActiveProfileNotExistError,
  ActorClass,
  DeesonetResponse,
  IncorrectStatusError,
  UnavailableLangError,
} from '../common';
import {
  ChatEnterState,
  CreateChatState,
  NicknameChoiceState,
  SetTranslatorState,
  States,
  StateService,
} from './states';

@Injectable()
export class ChatService {
  constructor(
    private v1: HelpService,
    private readonly stateService: StateService,
  ) {}

  public async getActor(
    authId: number,
    authType: AuthTypeEnum,
    meta?: unknown,
  ): Promise<{ actor: ActorClass; newUser: boolean }> {
    const user = await this.v1.getUser(authId, authType);
    let newUser = false;
    if (!user) {
      await this.v1.createUser(authId, authType, meta);
      newUser = true;
    }
    const actor = await this.v1.getActor(authId, authType);
    return {
      actor,
      newUser,
    };
  }

  async getChats(userId: number): Promise<string> {
    const chats = await this.v1.getChats();
    return MESSAGES.getMsgChats(chats);
  }

  async createChat(actor: ActorClass): Promise<string> {
    await this.stateService.set(actor.user.id, new CreateChatState());
    return MESSAGES.chatNameEnter();
  }

  async enterChat(authId: number, authType: AuthTypeEnum): Promise<string> {
    const actor = await this.v1.getActor(authId, authType);
    await this.stateService.set(actor.user.id, new ChatEnterState());
    return MESSAGES.enterChatId();
  }

  async exitChat(authId: number, authType: AuthTypeEnum): Promise<string> {
    const {
      actor: { user },
    } = await this.getActor(authId, authType);
    await this.v1.deactivateProfile(user.id);
    return MESSAGES.getMsgExitChat();
  }

  async message(
    authId: number,
    authType: AuthTypeEnum,
    msg: MessageClass,
  ): Promise<DeesonetResponse> {
    const actor = await this.v1.getActor(authId, authType);

    /*
     * Если мы получили сообщение, то мы должны проверить, какое у пользователя состояние
     * */
    const userState = await this.stateService.get(actor.user.id);
    if (userState) {
      // это очень плохо, но пока не придумал как красиво оформить
      switch (userState.key) {
        case States.chatEnter:
          const chatId = Number(msg.text);
          const chat = await this.v1.getChat(chatId);
          if (!chat) throw new ChatNotFoundException(chatId);
          const profile = await this.v1.getProfile(actor.user.id, chatId);
          if (profile) {
            await this.v1.activateProfile(profile.id);
            await this.stateService.delete(actor.user.id);
            return new DeesonetResponse(
              [actor],
              new MessageClass(MESSAGES.getMsgEnterChat(profile)),
            );
          }
          const newState = new NicknameChoiceState(chatId);
          await this.stateService.set(actor.user.id, newState);
          return new DeesonetResponse(
            [actor],
            new MessageClass(MESSAGES.getMsgEnterName()),
          );

        case States.nicknameChoice:
          const nickname = msg.text;
          if (nickname.length > MAX_NICKNAME_LENGTH)
            throw new NicknameTooLongError();
          const newProfile = await this.v1.createProfile(
            userState.metadata.chatId,
            actor.user.id,
            nickname,
          );
          await this.stateService.delete(actor.user.id);
          return new DeesonetResponse(
            [actor],
            new MessageClass(MESSAGES.getMsgNewProfile(newProfile)),
          );

        case States.createChat:
          const chatname = msg.text;
          const newChat = await this.v1.createChat(chatname);
          await this.stateService.delete(actor.user.id);
          return new DeesonetResponse(
            [actor],
            new MessageClass(MESSAGES.getMsgCreateChat(newChat)),
          );

        case States.setTranslator:
          const lang = msg.text.toLowerCase();
          if (!availableLang(lang)) throw new UnavailableLangError();
          actor.activeProfile = await this.v1.setTranslatorForProfile(
            actor.activeProfile.id,
            lang,
          );
          await this.stateService.delete(actor.user.id);
          return new DeesonetResponse(
            [actor],
            new MessageClass('Настройка применена.'),
          );

        default:
          await this.stateService.delete(actor.user.id);
          throw new IncorrectStatusError();
      }
    }
    // обычный флоу
    // Если нет активного профиля, то отправляем ему сообщение
    if (!actor.activeProfile) {
      throw new ActiveProfileNotExistError();
    }

    /**
     * Здесь мы уже точно знаем, что пользователь авторизирован и имеет активный профиль.
     * Значит просто отправляем сообщение в чат.
     * */
    // сохраняем сообщение в базу для истории
    const message = await this.v1.saveMessage(
      new MessageClass(msg.text),
      actor.account,
      actor.activeProfile,
    );
    // находим получателей. Список аккаунтов с активным профилем в этой комнате.
    const recipients = await this.v1.getRecipients(
      actor.activeProfile.chat.id,
      actor.activeProfile.id,
    );
    const fullText = `${actor.activeProfile.nickname}: ${message.data}`;
    const responseMessage = new MessageClass(fullText);
    responseMessage.image = msg.image;
    return new DeesonetResponse(recipients, responseMessage);
  }

  public async setTranslator(actor: ActorClass): Promise<DeesonetResponse> {
    if (!actor.activeProfile) {
      return new DeesonetResponse(
        [actor],
        new MessageClass('Сначала зайди в комнату'),
      );
    }

    await this.stateService.set(actor.user.id, new SetTranslatorState());
    return new DeesonetResponse(
      [actor],
      new MessageClass(MESSAGES.availableLang()),
    );
  }
}
