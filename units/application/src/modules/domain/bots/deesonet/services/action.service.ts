import {
  ActionDataClass,
  MessageClass,
  CatchBotException,
  MessageQueueService,
} from '@deesonet/common';
import { Injectable } from '@nestjs/common';
import { ChatService } from './chat.service';
import { MESSAGES } from '../messages';
import { mapToAuthType } from '../../../../common';
import { ResponseService } from './response.service';
import { DeesonetResponse } from '../common';

@Injectable()
export class ActionService {
  constructor(
    private readonly chatService: ChatService,
    private readonly responseService: ResponseService,
    private readonly queue: MessageQueueService,
  ) {}

  @CatchBotException()
  public async start(data: ActionDataClass) {
    const { actor } = await this.chatService.getActor(
      data.authId,
      mapToAuthType(data.clientName),
      data.meta,
    );
    new MessageClass(
      MESSAGES.getMsgHelloDeesonet(
        data.authId,
        actor.user.id,
        mapToAuthType(data.clientName),
      ),
    );
  }

  @CatchBotException()
  public async newMessage(data: ActionDataClass) {
    const { actor, newUser } = await this.chatService.getActor(
      data.authId,
      mapToAuthType(data.clientName),
      data.meta,
    );

    // если новый пользователь - отправляем приветственное сообщение
    if (newUser) {
      await this.responseService.put(
        new DeesonetResponse(
          [actor],
          new MessageClass(
            MESSAGES.getMsgHelloDeesonet(
              data.authId,
              actor.user.id,
              mapToAuthType(data.clientName),
            ),
          ),
        ),
      );
      // если не новый - идем по обычному флоу
    } else {
      const resp2 = await this.chatService.message(
        data.authId,
        mapToAuthType(data.clientName),
        data.message,
      );
      if (resp2) await this.responseService.put(resp2);
    }
  }

  @CatchBotException()
  public async help(data: ActionDataClass) {
    const { actor } = await this.chatService.getActor(
      data.authId,
      mapToAuthType(data.clientName),
      data.meta,
    );
    await this.responseService.put(
      new DeesonetResponse(
        [actor],
        new MessageClass(
          MESSAGES.getMsgHelloDeesonet(
            data.authId,
            actor.user.id,
            mapToAuthType(data.clientName),
          ),
        ),
      ),
    );
  }

  @CatchBotException()
  public async createChat(data: ActionDataClass) {
    const { actor } = await this.chatService.getActor(
      data.authId,
      mapToAuthType(data.clientName),
      data.meta,
    );
    const message = await this.chatService.createChat(actor);
    await this.responseService.put(
      new DeesonetResponse([actor], new MessageClass(message)),
    );
  }

  @CatchBotException()
  public async getChats(data: ActionDataClass) {
    const { actor } = await this.chatService.getActor(
      data.authId,
      mapToAuthType(data.clientName),
      data.meta,
    );
    const message = await this.chatService.getChats(0);
    await this.responseService.put(
      new DeesonetResponse([actor], new MessageClass(message)),
    );
  }

  @CatchBotException()
  public async enterChat(data: ActionDataClass) {
    const { actor } = await this.chatService.getActor(
      data.authId,
      mapToAuthType(data.clientName),
      data.meta,
    );
    const message = await this.chatService.enterChat(
      data.authId,
      mapToAuthType(data.clientName),
    );
    await this.responseService.put(
      new DeesonetResponse([actor], new MessageClass(message)),
    );
  }

  @CatchBotException()
  public async setTranslator(data: ActionDataClass) {
    const { actor } = await this.chatService.getActor(
      data.authId,
      mapToAuthType(data.clientName),
      data.meta,
    );
    const message = await this.chatService.setTranslator(actor);
    await this.responseService.put(message);
  }

  @CatchBotException()
  public async exitChat(data: ActionDataClass) {
    const { actor } = await this.chatService.getActor(
      data.authId,
      mapToAuthType(data.clientName),
      data.meta,
    );
    const message = await this.chatService.exitChat(
      data.authId,
      mapToAuthType(data.clientName),
    );
    await this.responseService.put(
      new DeesonetResponse([actor], new MessageClass(message)),
    );
  }
}
