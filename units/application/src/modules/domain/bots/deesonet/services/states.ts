import { Injectable } from '@nestjs/common';

export enum States {
  chatEnter = 'chatEntering',
  nicknameChoice = 'nicknameChoice',
  createChat = 'createChat',
  setTranslator = 'setTranslator',
}

export abstract class State {
  key: States;
  public metadata: Record<string, any>;

  protected constructor(key: States) {
    this.key = key;
  }
}

export class ChatEnterState extends State {
  key: States.chatEnter;
  constructor() {
    super(States.chatEnter);
  }
}

export class CreateChatState extends State {
  key: States.createChat;
  constructor() {
    super(States.createChat);
  }
}

export class NicknameChoiceState extends State {
  key: States.nicknameChoice;
  metadata: { chatId: number };

  constructor(chatId: number) {
    super(States.nicknameChoice);
    this.metadata = { chatId };
  }
}

export class SetTranslatorState extends State {
  key: States.setTranslator;
  constructor() {
    super(States.setTranslator);
  }
}

const stateChain: [{ from: States; to: States }] = [
  {
    from: States.chatEnter,
    to: States.nicknameChoice,
  },
];

@Injectable()
export class StateRepo {
  private entries: Map<number, State>;

  constructor() {
    this.entries = new Map();
  }

  public async get(userId): Promise<State | null> {
    return this.entries.get(userId);
  }

  public async set(userId: number, state: State) {
    return this.entries.set(userId, state);
  }

  public async delete(userId: number) {
    this.entries.delete(userId);
  }
}

@Injectable()
export class StateService {
  constructor(private readonly repo: StateRepo) {}
  public async get(userId): Promise<State> {
    return this.repo.get(userId);
  }
  public async set(userId, state: State) {
    const current = await this.get(userId);
    if (current) {
      const chain = stateChain.find(
        (el) => el.from === current.key && el.to === state.key,
      );
      if (!chain) {
        console.log(`Unable move from ${current.key} to ${state.key}`);
        throw 'Невозможно выполнить действие.';
      }
    }
    return this.repo.set(userId, state);
  }
  public async delete(userId: number) {
    return this.repo.delete(userId);
  }
}
