import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { WebControlller } from './controllers/web.controlller';
import { NewVkController } from './controllers/new-vk.controller';
import { NewTelegramController } from './controllers/new-telegram.controller';
import { ActionService } from './services/action.service';
import { ChatService } from './services/chat.service';
import { HelpService } from './services/help.service';
import { DeesonetConfigs } from './deesonet.configs';
import { TelegramClient, VkClient } from '@deesonet/libs';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DeesonetModels } from '../../../infra/models/deesonet';
import { DEESONET_TG_CLIENT, DEESONET_VK_CLIENT } from './constants';
import { AuthTypeEnum } from '../../../common';
import { DeesonetModule as DeesonetBotsModule } from '@deesonet/nest';
import { ResponseService } from './services/response.service';
import { StateRepo, StateService } from './services/states';
import { YandexClientService } from './services/yandex-client.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([...DeesonetModels]),
    ConfigModule,
    DeesonetBotsModule.forRoot([
      {
        class: TelegramClient,
        nestContextToken: DEESONET_TG_CLIENT,
        name: AuthTypeEnum.TELEGRAM,
      },
      {
        class: VkClient,
        nestContextToken: DEESONET_VK_CLIENT,
        name: AuthTypeEnum.VK,
      },
    ]),
  ],
  controllers: [WebControlller],
  providers: [
    NewVkController,
    NewTelegramController,
    ActionService,
    ChatService,
    HelpService,
    DeesonetConfigs,
    ResponseService,
    StateService,
    StateRepo,
    YandexClientService,
  ],
})
export class DeesonetModule {}
