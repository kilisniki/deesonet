import { ChatModel } from '../../../infra/models/deesonet/chat.model';
import { ProfileModel } from '../../../infra/models/deesonet/profile.model';
import { AuthTypeEnum } from '../../../common';

export const MESSAGES = {
  hello: 'hello',
  noActiveProfileMsg: () => {
    return 'У тебя нет активного чата, чика.\n/getchats - получить список чатов';
  },
  getMsgHelloDeesonet: (
    authId: number,
    userId: number,
    authType: AuthTypeEnum,
  ) => {
    return (
      'Добро пожаловать в глубокую сеть. \n' +
      `Вход через ${authType}, твой ${authType} Id <${authId}>. Ваш userId в deesonet <${userId}>\n` +
      MESSAGES.getMsgHelp()
    );
  },
  getMsgHelp: () => {
    return `/help - стартовый экран
/getchats - выводит список id и название комнаты
/enterchat - вход в комнату по id, номера выводятся в списке
/createchat - создание чата
/exitchat - выход из комнаты
/translator - сменить язык перевода`;
  },
  getMsgChats: (chats: ChatModel[]) => {
    return `Доступные чаты: \n ${chats.reduce((acc, current) => {
      acc = acc + `${current.id} : ${current.name}\n`;
      return acc;
    }, '')}`;
  },
  chatNameEnter: () => {
    return 'Введи название комнаты';
  },
  availableLang: () => {
    return (
      'Выбери язык на который нужно переводить сообщения. ' +
      'Поддерживается стандартный формат: ru, en, es и т.д. Напиши "null" если хочешь выключить фичу.'
    );
  },
  getMsgCreateChat: (chat: ChatModel) => {
    return `Чат создан <${chat.name}>, id: ${chat.id}`;
  },
  enterChatId: () => {
    return 'Введи номер комнаты:';
  },
  getMsgEnterName: () => {
    return `Введи никнейм для комнаты:`;
  },
  getMsgEnterChat: (profile: ProfileModel) => {
    return `Ты вошел в комнату как ${profile.nickname}`;
  },
  getMsgCreateProfile: (chat: ChatModel) => {
    return `Введи никнейм для комнаты ${chat.name}`;
  },
  getMsgNewProfile: (profile: ProfileModel) => {
    return `Твой никнейм для комнаты - ${profile.nickname}`;
  },
  getMsgExitChat: () => {
    return `Ты вышел из всех комнат`;
  },
};
