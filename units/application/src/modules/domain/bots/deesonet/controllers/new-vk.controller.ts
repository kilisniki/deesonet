import { ChatService } from '../services/chat.service';
import { Inject, Injectable } from '@nestjs/common';
import { VkClient } from '@deesonet/libs';
import { ActionService } from '../services/action.service';
import { DEESONET_VK_CLIENT } from '../constants';

@Injectable()
export class NewVkController {
  constructor(
    private readonly chatService: ChatService,
    @Inject(DEESONET_VK_CLIENT) private readonly vkClient: VkClient,
    private readonly actionService: ActionService,
  ) {
    this.configuration();
    this.vkClient.init().then().catch(console.error);
  }

  configuration() {
    this.vkClient.addCommandRoute(
      '/start',
      this.actionService.start.bind(this.actionService),
    );
    this.vkClient.addCommandRoute(
      '/help',
      this.actionService.help.bind(this.actionService),
    );
    this.vkClient.addCommandRoute(
      '/createchat',
      this.actionService.createChat.bind(this.actionService),
    );
    this.vkClient.addCommandRoute(
      '/getchats',
      this.actionService.getChats.bind(this.actionService),
    );
    this.vkClient.addCommandRoute(
      '/enterchat',
      this.actionService.enterChat.bind(this.actionService),
    );
    this.vkClient.addCommandRoute(
      '/exitchat',
      this.actionService.exitChat.bind(this.actionService),
    );
    this.vkClient.addCommandRoute(
      '/translator',
      this.actionService.setTranslator.bind(this.actionService),
    );
    this.vkClient.addMessageRoute(
      this.actionService.newMessage.bind(this.actionService),
    );
  }
}
