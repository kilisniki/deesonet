import { Inject, Injectable } from '@nestjs/common';
import { TelegramClient } from '@deesonet/libs';
import { HelpService } from '../services/help.service';
import { ChatService } from '../services/chat.service';
import { DeesonetConfigs } from '../deesonet.configs';
import { ActionService } from '../services/action.service';
import { DEESONET_TG_CLIENT } from '../constants';

@Injectable()
export class NewTelegramController {
  constructor(
    private v1: HelpService,
    private chatService: ChatService,
    private readonly config: DeesonetConfigs,
    @Inject(DEESONET_TG_CLIENT) private readonly client: TelegramClient,
    private readonly actionService: ActionService,
  ) {
    this.configuration();
    this.client.init();
  }

  private configuration() {
    this.client.addCommandRoute(
      /\/start(.*)/,
      this.actionService.start.bind(this.actionService),
    );
    this.client.addCommandRoute(
      /\/help(.*)/,
      this.actionService.help.bind(this.actionService),
    );
    this.client.addCommandRoute(
      /\/createchat(.*)/,
      this.actionService.createChat.bind(this.actionService),
    );
    this.client.addCommandRoute(
      /\/getchats(.*)/,
      this.actionService.getChats.bind(this.actionService),
    );
    this.client.addCommandRoute(
      /\/enterchat(.*)/,
      this.actionService.enterChat.bind(this.actionService),
    );
    this.client.addCommandRoute(
      /\/exitchat(.*)/,
      this.actionService.exitChat.bind(this.actionService),
    );
    this.client.addCommandRoute(
      /\/translator(.*)/,
      this.actionService.setTranslator.bind(this.actionService),
    );
    this.client.addMessageRoute(
      this.actionService.newMessage.bind(this.actionService),
    );
  }
}
