import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { UserModel } from './user.model';
import { AuthTypeEnum } from '../../../common';
import { MessageModel } from './message.model';

@Entity()
export class AccountModel {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  authId: number;

  @Column({
    enum: AuthTypeEnum,
  })
  authType: AuthTypeEnum;

  @ManyToOne(() => UserModel, (user) => user.accounts)
  user: UserModel;

  @CreateDateColumn()
  creationAt: Date;

  @Column({
    type: 'json',
    nullable: true,
  })
  meta?: unknown;

  @OneToMany(() => MessageModel, (message) => message.account)
  messages: MessageModel[];
}
