import { AccountModel } from './account.model';
import { ChatModel } from './chat.model';
import { MessageModel } from './message.model';
import { ProfileModel } from './profile.model';
import { UserModel } from './user.model';

export const DeesonetModels = [
  AccountModel,
  ChatModel,
  MessageModel,
  ProfileModel,
  UserModel,
];
