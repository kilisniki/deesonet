import {
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { AccountModel } from './account.model';
import { ProfileModel } from './profile.model';

@Entity()
export class UserModel {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToMany(() => AccountModel, (account) => account.user)
  accounts: AccountModel[];

  @OneToMany(() => ProfileModel, (profile) => profile.user)
  profiles: ProfileModel[];

  @CreateDateColumn()
  registrationAt: Date;
}
