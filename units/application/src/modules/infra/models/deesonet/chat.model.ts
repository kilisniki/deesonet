import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ProfileModel } from './profile.model';

@Entity()
export class ChatModel {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    unique: true,
  })
  name: string;

  @Column({
    default: true,
  })
  active: boolean;

  @OneToMany(() => ProfileModel, (profile) => profile.chat)
  profiles: ProfileModel[];

  @CreateDateColumn()
  creationAt: Date;

  /* @OneToMany(() => MessageModel, (message) => message.chat)
  messages: MessageModel; */
}
