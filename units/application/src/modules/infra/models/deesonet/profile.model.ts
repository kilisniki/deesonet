import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';
import { UserModel } from './user.model';
import { ChatModel } from './chat.model';
import { MessageModel } from './message.model';
import { MAX_NICKNAME_LENGTH } from '../../../domain/bots/deesonet/constants';
import { ProfileSettings } from '../../../domain/bots/deesonet/common';

@Entity()
//@Unique(['user', 'chat'])
@Unique(['chat', 'nickname'])
export class ProfileModel {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: MAX_NICKNAME_LENGTH,
    nullable: false,
  })
  nickname: string;

  @Column({
    default: true,
  })
  active: boolean;

  @Column({
    type: 'jsonb',
    nullable: true,
  })
  settings?: ProfileSettings;

  @ManyToOne(() => UserModel, (user) => user.profiles)
  user: UserModel;
  // userId?: number;

  @ManyToOne(() => ChatModel, (chat) => chat.profiles)
  chat: ChatModel;
  // chatId?: number;

  @CreateDateColumn()
  creationAt: Date;

  @OneToMany(() => MessageModel, (message) => message.profile)
  messages: MessageModel[];
}
