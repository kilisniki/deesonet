import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ProfileModel } from './profile.model';
import { AccountModel } from './account.model';
import { ChatModel } from './chat.model';

@Entity()
export class MessageModel {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  data: string;

  /* @ManyToOne(() => ChatModel, (chat) => chat.messages)
  chat: ChatModel; */

  @ManyToOne(() => AccountModel, (account) => account.messages)
  account: AccountModel;

  @ManyToOne(() => ProfileModel, (profile) => profile.messages)
  profile: ProfileModel;

  @CreateDateColumn()
  creationAt: Date;
}
