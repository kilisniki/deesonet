import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AllModule } from './modules/all.module';
import { ConfigModule } from '@nestjs/config';
import { OrmConfig } from './ormconfig';

@Module({
  imports: [
    TypeOrmModule.forRoot(OrmConfig),
    ConfigModule.forRoot(),
    AllModule,
  ],
  controllers: [AppController],
})
export class AppModule {}
