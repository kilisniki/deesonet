import { config } from 'dotenv';
import { TypeOrmModuleOptions } from '@nestjs/typeorm/dist/interfaces/typeorm-options.interface';

config();

export const OrmConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB,
  synchronize: true,
  logging: false,
  entities: [
    'modules/infra/models/deesonet/*.model.js',
    'build/modules/infra/models/deesonet/*.model.js',
    'units/application/build/modules/infra/models/deesonet/*.model.js',
  ],
  //migrations: ['src/migration/**/*.ts'],
  //subscribers: ['src/subscriber/**/*.ts'],
  // cli: {
  //   entitiesDir: 'src/entity',
  //   migrationsDir: 'src/migration',
  //   subscribersDir: 'src/subscriber',
  // },
};
