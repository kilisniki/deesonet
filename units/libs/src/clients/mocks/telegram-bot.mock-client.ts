import {
  ApiClient,
  ClientTask,
  ActionFunction,
  ApiClientConfig,
  Providers,
} from '@deesonet/common';

export class TelegramBotMockClient extends ApiClient {
  constructor(configs: ApiClientConfig) {
    super(configs, Providers.TG);
  }

  addCommandRoute(route: RegExp | string, func: ActionFunction): any {
    console.log(`add TG MOCK command route: ${route}`);
  }

  addMessageRoute(func: ActionFunction): any {
    console.log(`add TG MOCK message route`);
  }

  async init(): Promise<any> {
    console.log(`TG MOCK CLIENT initialize`);
  }

  sendMessage(task: ClientTask): any {
    console.log(
      `TG MOCK: new message <${task.message.text}> for ${task.recipients}`,
    );
  }
}
