import {
  ActionDataClass,
  ClientTask,
  ApiClient,
  ActionFunction,
  ApiClientConfig,
  Providers,
} from '@deesonet/common';

export class VkBotMockClient extends ApiClient {
  constructor(config: ApiClientConfig) {
    super(config, Providers.VK);
  }

  addCommandRoute(route: RegExp | string, func: ActionFunction): any {
    console.log(`add VK MOCK command route: ${route}`);
  }

  addMessageRoute(func: ActionFunction): any {
    console.log(`add VK MOCK message route`);
  }

  async init(): Promise<any> {
    console.log(`VK bot MOCK client initialize`);
  }

  sendMessage(task: ClientTask): any {
    console.log(
      `VKMOCK: new message <${task.message.text}> for ${task.recipients}`,
    );
  }
}
