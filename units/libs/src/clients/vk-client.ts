import {
  ApiClient,
  ActionDataClass,
  ClientTask,
  MessageClass,
  ActionFunction,
  ApiClientConfig,
  Providers,
} from '@deesonet/common';
import { VkBotAttachment, VkBotType } from '../types/vk-bot-request.type';
import * as VkBot from 'node-vk-bot-api';
import * as crypto from 'crypto';
import * as path from 'path';
import { downloadFile } from '../helpers';
import { VkHelperClient } from './vk-helper-client';

export class VkClient extends ApiClient {
  private bot: VkBotType;
  private helperClient: VkHelperClient;

  constructor(config: ApiClientConfig) {
    super(config, Providers.VK);
    // @ts-ignore
    this.bot = new VkBot(config.token);
  }

  addMessageRoute(action: ActionFunction) {
    this.bot.on(async (ctx) => {
      const message = new MessageClass(ctx.message?.body);
      if (ctx.message?.attachments?.length) {
        message.image = await this.getImageFromMessage(
          ctx.message?.attachments,
        );
      }
      if (ctx.message?.body[0] === '/') return;
      const data = new ActionDataClass(
        this.name,
        this.provider,
        ctx.message?.user_id,
        message,
        ctx.message,
      );

      await action(data);
    });
  }

  addCommandRoute(route: string, action: ActionFunction) {
    this.bot.command(route, async (ctx) => {
      const data = new ActionDataClass(
        this.name,
        this.provider,
        ctx.message?.user_id,
        new MessageClass(ctx.message?.body.split(' ').pop()),
        ctx,
      );

      await action(data);
    });
  }

  async init() {
    this.bot.startPolling((err) => {
      if (err) {
        console.error(err);
      }
      return err;
    });
    this.helperClient = new VkHelperClient();
    await this.helperClient.init(this.config.token);
  }

  // TODO переделать на userId, messages: string[]
  async sendMessage(task: ClientTask) {
    const promises = [];
    for (const rec of task.recipients) {
      promises.push(this.bot.sendMessage(rec, task.message?.text));
      if (task.message?.image) {
        try {
          const id = await this.helperClient.uploadPhoto(task.message?.image);
          // Здесь раньше отправлялся текст вместе с фото,
          // не помню зачем, зафиксил пока что
          await this.bot.sendMessage(rec, /* task.message.text */ '', id);
        } catch (e) {
          console.error(e);
        }
      }
    }
    await Promise.all(promises);
  }

  private async getImageFromMessage(
    attachment: VkBotAttachment[],
  ): Promise<string> {
    const photo = attachment.find((el) => el.type === 'photo');
    if (!photo) return;

    const link = photo.photo['photo_1280'];
    const fileName = crypto
      .createHash('md5')
      .update(Date.now().toString())
      .digest('hex');
    const newPath = path.join(this.config.tmpDir, `vkFile_${fileName}.jpg`);
    await downloadFile(link, newPath);
    return newPath;
  }
}
