import * as easyvk from 'easyvk';

export class VkHelperClient {
  private vk;
  private client;
  private uploader;

  public async init(token: string) {
    this.vk = await easyvk({
      token,
      reauth: true,
      mode: 'highload' || {
        timeout: 10,
        name: 'highload',
      },
      utils: {
        uploader: true,
      },
    });
    // this.client = await this.vk.bots.longpoll.connect();
    this.uploader = await this.vk.uploader;

    // this.client.on('message_new', console.log);
  }

  // VK Developer, I hate you
  public async uploadPhoto(path: string): Promise<string> {
    const getUploadUrlRoute = 'photos.getMessagesUploadServer';
    const savePhotoRoute = 'photos.saveMessagesPhoto';
    const url = await this.uploader.getUploadURL(getUploadUrlRoute, {
      /* group_id */
    });
    const fileData = await this.uploader.uploadFile(url, path);
    const savedFiles = await this.vk.post(savePhotoRoute, fileData);
    const savedFile = savedFiles.pop();
    return `photo${savedFile.owner_id}_${savedFile.id}_${savedFile.access_key}`;
  }
}
