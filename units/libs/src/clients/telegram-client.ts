import {
  ApiClient,
  ActionDataClass,
  ClientTask,
  MessageClass,
  ActionFunction,
  ApiClientConfig,
  Providers,
} from '@deesonet/common';
import * as TelegramBot from 'node-telegram-bot-api';
import * as path from 'path';
import * as crypto from 'crypto';
import { downloadFile } from '../helpers';

export class TelegramClient extends ApiClient {
  private api: TelegramBot;

  constructor(config: ApiClientConfig) {
    super(config, Providers.TG);
    this.api = new TelegramBot(config.token, { polling: true });
    // this.api.sendMessage(222215695, 'hello pidor')
  }

  addCommandRoute(route: RegExp, func: ActionFunction) {
    this.api.onText(route, async (msg, match) => {
      const data = new ActionDataClass(
        this.name,
        this.provider,
        msg.chat.id,
        new MessageClass(match[1]),
        msg,
      );
      await func(data);
    });
  }

  addMessageRoute(func: (data: ActionDataClass) => Promise<void>) {
    this.api.on('message', async (msg, match) => {
      if (msg.text && msg.text[0] === '/') return;
      console.log(msg);
      const message = new MessageClass(msg.text);
      if (msg.photo?.length) {
        message.image = await this.loadPhoto(
          msg.photo[msg.photo.length - 1].file_id,
        );
      }
      if (msg.document) {
        message.files = [
          await this.loadPhoto(
            msg.document.file_id,
            msg.document.file_name.split('.').pop(),
          ),
        ];
      }
      const data = new ActionDataClass(
        this.name,
        this.provider,
        msg.chat.id,
        message,
        msg,
      );
      await func(data);
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  init() {}

  async sendMessage(task: ClientTask) {
    const promises = [];
    for (const rec of task.recipients) {
      promises.push(this.api.sendMessage(rec, task.message.text));
      if (task.message.image) {
        promises.push(this.api.sendPhoto(rec, task.message.image));
      }
      if (task.message.files) {
        for (const file of task.message.files) {
          promises.push(this.api.sendDocument(rec, file));
        }
      }
    }
    await Promise.all(promises);
  }

  async loadPhoto(fileId: string, format = 'jpg'): Promise<string> {
    const link = await this.api.getFileLink(fileId);
    const fileName = crypto
      .createHash('md5')
      .update(Date.now().toString())
      .digest('hex');
    const newPath = path.join(
      this.config.tmpDir,
      `tgFile_${fileName}.${format}`,
    );
    await downloadFile(link, newPath);
    return newPath;
  }
}
