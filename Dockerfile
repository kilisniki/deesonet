# Этап сборки
FROM node:22-alpine as build

WORKDIR /app

# Копируем package.json и package-lock.json отдельно для кэширования npm install
COPY package*.json ./

# Копируем все остальные файлы
COPY . .

# Удаляем ненужные node_modules и build директории
RUN rm -rf ./node_modules \
    && rm -rf ./units/application/node_modules \
    && rm -rf ./units/application/build \
    && rm -rf ./units/common/node_modules \
    && rm -rf ./units/common/build \
    && rm -rf ./units/libs/node_modules \
    && rm -rf ./units/libs/build \
    && rm -rf ./units/nest/node_modules \
    && rm -rf ./units/nest/build \
    && rm -rf ./bot/dist

# Выполняем сборку проекта
RUN npm install || true
RUN npm run build || true && npm run build

# Этап производства
FROM node:22-alpine as production

USER node
WORKDIR /app

# Копируем необходимые файлы из этапа сборки
COPY --chown=node:node --from=build /app/package*.json ./
COPY --chown=node:node --from=build /app/units/application/build /app/units/application/build
COPY --chown=node:node --from=build /app/units/application/package.json /app/units/application/package.json
COPY --chown=node:node --from=build /app/units/common/build /app/units/common/build
COPY --chown=node:node --from=build /app/units/common/package.json /app/units/common/package.json
COPY --chown=node:node --from=build /app/units/libs/build /app/units/libs/build
COPY --chown=node:node --from=build /app/units/libs/package.json /app/units/libs/package.json
COPY --chown=node:node --from=build /app/units/nest/build /app/units/nest/build
COPY --chown=node:node --from=build /app/units/nest/package.json /app/units/nest/package.json

# Устанавливаем только production зависимости
RUN npm install --only=production -w application

ARG COMMIT_TAG="none"
LABEL COMMIT_TAG=$COMMIT_TAG

EXPOSE 4000
ENTRYPOINT ["node", "units/application/build/main.js"]
